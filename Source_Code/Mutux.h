#ifndef _MUTUX_H
#define _MUTUX_H




// Define a mutex handle
SemaphoreHandle_t xMutex;

// Shared resource protected by the mutex
int sharedVariable = 0;

void vTask1(void *pvParameters) {

    while (1) {
        // Attempt to take the mutex
        if (xSemaphoreTake(xMutex, portMAX_DELAY)) {
            // Critical section: Access the shared resource
            sharedVariable++;
             PORT.P9 = 0x0; 
            // Release the mutex
            xSemaphoreGive(xMutex);
        }

        // Task delay or other non-critical code
         vTaskDelay(1 / portTICK_RATE_MS);
    }
}

void vTask2(void *pvParameters) {
    while (1) {
        // Attempt to take the mutex
        if (xSemaphoreTake(xMutex, portMAX_DELAY)) {
            // Critical section: Access the shared resource
            sharedVariable--;
             PORT.P9 =0x0001;
            // Release the mutex
            xSemaphoreGive(xMutex);
        }

        // Task delay or other non-critical code
         vTaskDelay(1 / portTICK_RATE_MS);
    }
}

int Create_Mutux(void) {
    // Initialize FreeRTOS and create the mutex
    xMutex = xSemaphoreCreateMutex();

    if (xMutex != NULL) {
        // Create tasks
        xTaskCreate(vTask1, "Task1", configMINIMAL_STACK_SIZE, NULL, 1, NULL);
        xTaskCreate(vTask2, "Task2", configMINIMAL_STACK_SIZE, NULL, 1, NULL);

 
    }

    // Will not reach here unless there is an error
    return 0;
}

#endif