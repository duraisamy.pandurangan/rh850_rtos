#include "iodefine.h"
#include "r_cg_userdefine.h"
#include "r_cg_macrodriver.h"
//**************************************************************************************************************************

//unsigned long ver;
//unsigned char ver1;
//unsigned char my_i;
extern unsigned char self_test_done;
extern unsigned char tx_done;
extern unsigned char rx_flag;
unsigned int i;

unsigned char RX_Fifo_empty,RX_fifo_full,RX_Fifo_msg_lost = 0; 

typedef struct 
{
unsigned char rx_msg_DLC;
unsigned int  rx_msg_ID;
unsigned long rx_msg_data0;   
unsigned long rx_msg_data1;
unsigned long rx_msg_data2;
unsigned long rx_msg_data3; 
unsigned long rx_msg_data4; 
unsigned long rx_msg_data5;
unsigned long rx_msg_data6;
unsigned long rx_msg_data7; 
   



}RX_fifo_data;


//**************************************************************************************************************************
void can_init(void)
{
//---------- Set CAN0 port pins -------------------
//for F1KM S4 
// CAN0TX --> P1_3
// CAN0RX --> P1_2

   //PORT.PMC1 |= 0x000C;//select alternate pin function for P1.2,P1.3
   //PORT.PM1 &= 0xFFF7; //set output mode for P1.3 as it is CAN3TX
   //PORT.PM1 |= 0x0004; //set  input mode for P1.2 as it is CAN3RX

//for F1KM S1 
// CAN0TX --> P0_0
// CAN0RX --> P0_1
   PORT.PMC0 |= 0x0003;//select alternate pin function for P0.0,P0.1
   PORT.PM0 &= 0xFFFE; //set output mode for P0.0 as it is CAN0TX
   PORT.PM0 |= 0x0002; //set  input mode for P0.1 as it is CAN0RX
   
// CAN1TX --> P0_3
// CAN1RX --> P0_2
//   PORT.PMC0 |= 0x000C;//select alternate pin function for P0.2,P0.3
//   PORT.PM0 &= 0xFFF7; //set output mode for P0.3 as it is CAN1TX
//   PORT.PM0 |= 0x0004; //set  input mode for P0.2 as it is CAN1RX   

   PORT.PFC0 = 0x0003;
   PORT.PFCAE0 = 0x0000;
   PORT.PFCE0 = 0x0000;
   
   //-------Set CAN clock-----------------------------
   // CAN Clock devider = /1
   my_protected_write(WPROTR.PROTCMD1,CLKCTL.CKSC_ICANOSCD_CTL,0x01);
   while(CLKCTL.CKSC_ICANOSCD_ACT!=0x01);

   // PLL -> CAN Clock
   my_protected_write(WPROTR.PROTCMD1,CLKCTL.CKSC_ICANS_CTL,0x01);
   while(CLKCTL.CKSC_ICANS_ACT!=0x01);
   
   // clk_xincan --> CKSCLK_ICANOSC --> Main Osc fx/1--> 8Mhz 16MHz
   // clkc --> PPLLCLK2 --> 40MHz
   // pclk --> CKSCLK_ICAN --> CPUCLK --> 80MHz
   
   
   //-----------------------------------------------
   //GRAMINIT = 0; initiallizatoin finished
   //GRAMINIT = 1; initiallizatoin ongoing

   //Global Status Register
   while((RCFDC0.CFDGSTS.UINT8[LL] && 0x04)==0x04); //wait until CAN RAM is
                                                    //initialized
                                                    //global stop status flag
   //Global Control Register 
   RCFDC0.CFDGCTR.UINT8[LL]      &= 0xFB;   //Transition to global reset mode 
                                            //from global stop mode
               //GSLPR-->0 other than stop mode
               //GSLPR-->1 stop mode
   //Channel Control Register
   RCFDC0.CFDC0CTR.UINT8[LL] &= 0xFB;   //Transition to channel reset mode from channel stop mode
               //CSLPR-->0 other than stop mode
               //CSLPR-->1 stop mode
   
//-------- Baud settings for 40MHz clock --------------------------------------
#if 0
   
   RSCAN0.GCFG.UINT32 = 0x00;      // timestamp, clock selection(clkc-->40MHz), Priority on ID

   //RSCAN0.C0CFG.UINT32 = 0x00230027;   // 125kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140027;   // 125kbps, 8Tq, 1,2,5, 75% sampling
   
   //RSCAN0.C0CFG.UINT32 = 0x00230013;   // 250kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140013;   // 250kbps, 8Tq, 1,2,5, 75% sampling
   
   //RSCAN0.C0CFG.UINT32 = 0x00230009;   // 500kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140009;   // 500kbps, 8Tq, 1,2,5, 75% sampling
   
   //RSCAN0.C0CFG.UINT32 = 0x00230004;   // 1Mbps, 8Tq, 1,3,4, 62.5% sampling
   RSCAN0.C0CFG.UINT32 = 0x00140004;   // 1Mbps, 8Tq, 1,2,5, 75% sampling
#endif

//---------- Baud settings for 16MHz clock -------------------------------------
#if 0   
        //Global Configuration Register
   RCFDC0.CFDGCFG.UINT32 = 0x10;   //xin clock selected
   
   //RSCAN0.C0CFG.UINT32 = 0x00230007;   // 125kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140007;   // 125kbps, 8Tq, 1,2,5, 75% sampling
   
   //Channel CAN FD Configuration Register
   RCFDC0.CFDC0FDCFG.UINT8[HH] = 0x40;     //Classical CAN only mode is enabled
// RCFDC0.CFDC0FDCFG.UINT8[HH] = 0x10;     //CAN-FD only mode is enabled    
   
   //Channel Data Bit Rate Configuration Register
   //RCFDC0.CFDC0DCFG.UINT32 = 0x00230001;   // 500kbps, 8Tq, 1,3,4, 62.5% sampling
   //RCFDC0.CFDC0NCFG.UINT32 = 0x02030001;   // 500kbps, 8Tq, 1,3,4, 62.5% sampling
   RCFDC0.CFDC0NCFG.UINT32 = 0x04090801;     // 500kbps, 16Tq, 1,4,9, 68.75% sampling
                              
   
   //RCFDC0.CFDC0FDCFG.UINT32 = 0x00230003;   // 250kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140003;   // 250kbps, 8Tq, 1,2,5, 75% sampling
   
   //RSCAN0.C0CFG.UINT32 = 0x00230001;   // 500kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140001;   // 500kbps, 8Tq, 1,2,5, 75% sampling
   
   //RSCAN0.C0CFG.UINT32 = 0x00230000;   // 1Mbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140000;   // 1Mbps, 8Tq, 1,2,5, 75% sampling
   
#endif   

//---------- Baud settings for 8MHz clock -------------------------------------
#if 1   
        //Global Configuration Register
   RCFDC0.CFDGCFG.UINT32 = 0x10;   //xin clock selected
   
   //RSCAN0.C0CFG.UINT32 = 0x00230007;   // 125kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140007;   // 125kbps, 8Tq, 1,2,5, 75% sampling
   
   //Channel CAN FD Configuration Register
   RCFDC0.CFDC0FDCFG.UINT8[HH] = 0x40;     //Classical CAN only mode is enabled
// RCFDC0.CFDC0FDCFG.UINT8[HH] = 0x10;     //CAN-FD only mode is enabled  
   
   //Channel Data Bit Rate Configuration Register
   RCFDC0.CFDC0NCFG.UINT32 = 0x02030001;   // 500kbps, 8Tq, 1,3,4, 62.5% sampling
                              
   
   //RCFDC0.CFDC0FDCFG.UINT32 = 0x00230003;   // 250kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140003;   // 250kbps, 8Tq, 1,2,5, 75% sampling
   
   //RSCAN0.C0CFG.UINT32 = 0x00230001;   // 500kbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140001;   // 500kbps, 8Tq, 1,2,5, 75% sampling
   
   //RSCAN0.C0CFG.UINT32 = 0x00230000;   // 1Mbps, 8Tq, 1,3,4, 62.5% sampling
   //RSCAN0.C0CFG.UINT32 = 0x00140000;   // 1Mbps, 8Tq, 1,2,5, 75% sampling
   
#endif   

//--------- Receive Rule settings for RX FIFO -------------------------------------
#if 1
        //Receive Rule Configuration Register
   //RSCAN0.GAFLCFG0.UINT8[3] = 0x02;   // No. of rules for channel 0
   RCFDC0.CFDGAFLCFG0.UINT8[HH] = 0x04;    // No. of rules for channel 0 //4
   // No. of rules for channel 3 CAN0TX/RX channel
   //Receive rule entry control register
   //RSCAN0.GAFLECTR.UINT8[1] = 0x01;   // Enable write to receive rule table
   RCFDC0.CFDGAFLECTR.UINT8[LH] = 0x01;   // Enable write to receive rule table   
   //RSCAN0.GAFLECTR.UINT8[0] = 0x00;   // receive rule page no.configuration
   RCFDC0.CFDGAFLECTR.UINT8[LL] = 0x00;   // receive rule page no.configuration
         
// receive rule 1
        //Receive Rule ID Register
   //RSCAN0.GAFLID0.UINT16[0] = 0x0111;   // Standard, Data frame, 11 bit ID
   RCFDC0.CFDGAFLID0.UINT16[L] = 0x0111;   // Standard, Data frame, 11 bit ID
   //RSCAN0.GAFLM0.UINT32 = 0x00;      // ID bits are not compared compared
   //Receive Rule Mask Register
   //RSCAN0.GAFLM0.UINT32 = 0xff;      // ID bits are compared compared
   RCFDC0.CFDGAFLM0.UINT32 = 0x1FFFFFFF;   // ID bits are compared compared
   //Receive Rule Pointer 0 Register
   //RSCAN0.GAFLP00.UINT8[1] = 0x00;   
   RCFDC0.CFDGAFLP0_0.UINT8[LH] = 0x80;   //Use messsage buffer no 0
// RCFDC0.CFDGAFLP0_0.UINT8[LL] = 0x08;   //DLC 8 bytes
   RCFDC0.CFDGAFLP0_0.UINT8[LL] = 0x0F;   //DLC 64 bytes
   
   //Receive Rule Pointer 1 Register
   //RSCAN0.GAFLP10.UINT32 = 0x01;      // Receive FIFO 0 selected
   RCFDC0.CFDGAFLP1_0.UINT32 = 0x01;   // Receive FIFO 0 selected
   
// receive rule 2
        ////Receive Rule ID Register
   //RSCAN0.GAFLID1.UINT16[0] = 0x0222;   // Standard, Data frame, 11 bit ID
   RCFDC0.CFDGAFLID1.UINT16[L] = 0x0222;   // Standard, Data frame, 11 bit ID
   //RSCAN0.GAFLM0.UINT32 = 0x00;      // ID bits are not compared compared
   //Receive Rule Mask Register
   //RSCAN0.GAFLM1.UINT32 = 0xff;      // ID bits are compared compared
   RCFDC0.CFDGAFLM1.UINT32 = 0x1FFFFFFF;   // ID bits are compared compared
   //Receive Rule Pointer 0 Register
   //RSCAN0.GAFLP01.UINT8[1] = 0x00;   
   RCFDC0.CFDGAFLP0_1.UINT8[LH] = 0x80;    //Use messsage buffer no 0
// RCFDC0.CFDGAFLP0_1.UINT8[LL] = 0x08;   //DLC 8 bytes
   RCFDC0.CFDGAFLP0_1.UINT8[LL] = 0x0F;   //DLC 64 bytes
   
   //Receive Rule Pointer 1 Register
   //RSCAN0.GAFLP11.UINT32 = 0x01;      // Receive FIFO 0 selected
   RCFDC0.CFDGAFLP1_1.UINT32 = 0x01;       // Receive FIFO 0 selected
   
        //Receive rule entry control register
   //RSCAN0.GAFLECTR.UINT8[1] = 0x00;   // Disable write to receive rule table
   RCFDC0.CFDGAFLECTR.UINT8[LH] = 0x00;   // Disable write to receive rule table
   
   RCFDC0.CFDRFCC0.UINT16[L] = 0x1272;    // RFIGCV = xxx(don't care),RFIM = 1(An interrupt occurs each time a message has been received.)
                                          // RFDC =0x010 Receive FIFO Buffer Depth Configuration 
                      // RFPLS = 0x111 Receive FIFO Buffer Payload Storage Size Select
                      // RFIE =1 Receive FIFO Interrupt Enable
                      
//   RCFDC0.CFDRFCC0.UINT8[LL] = 0x72;      // RFPLS = 0x111 Receive FIFO Buffer Payload Storage Size Select
                      // RFIE =1 Receive FIFO Interrupt Enable
                      
//   RCFDC0.CFDRFCC0.UINT8[LH] = 0x12;      // RFIGCV = xxx(don't care)
                      // RFIM = 1(An interrupt occurs each time a message has been received.)
                                          // RFDC =0x010 Receive FIFO Buffer Depth Configuration 
#endif

//--------- Receive Rule settings for TX/RX FIFO -------------------------------------
#if 0
        //Receive Rule Configuration Register 0
   RCFDC0.CFDGAFLCFG0.UINT8[LL] = 0x02;   // No. of rules for channel 0
   //Receive Rule Entry Control Register
   RCFDC0.CFDGAFLECTR.UINT8[LH]= 0x01;   // Enable write to receive rule table
   //Receive rule entry control register
   RCFDC0.CFDGAFLECTR.UINT8[LL] = 0x00;   // receive rule page no.configuration
         
// receive rule 1
        //Receive Rule ID Register
   RCFDC0.CFDGAFLID0.UINT16[L] = 0x0111;   // Standard, Data frame, 11 bit ID
   //RSCAN0.GAFLM0.UINT32 = 0x00;      // ID bits are not compared compared
   //Receive Rule Mask Register
   RCFDC0.CFDGAFLM0.UINT32 = 0xffffffff;      // ID bits are compared compared
   //Receive Rule Pointer 0 Register
   RCFDC0.CFDGAFLP0_0.UINT8[1] = 0x00;
   //Receive Rule Pointer 1 Register
   RCFDC0.CFDGAFLP1_0.UINT32 = 0x0100;      // TX/RX FIFO 0 selected
   
// receive rule 2
        ////Receive Rule ID Register
   RCFDC0.CFDGAFLID1.UINT16[0] = 0x0222;   // Standard, Data frame, 11 bit ID
   //RSCAN0.GAFLM0.UINT32 = 0x00;      // ID bits are not compared compared
   //Receive Rule Mask Register
   RCFDC0.CFDGAFLM1.UINT32 = 0xffffffff;      // ID bits are compared compared
   //Receive Rule Pointer 0 Register
   RCFDC0.CFDGAFLP0_1.UINT8[1] = 0x00;
   //Receive Rule Pointer 1 Register
   RCFDC0.CFDGAFLP1_1.UINT32 = 0x0100;      // Receive FIFO 0 selected
        //Receive Rule Entry Control Register
   RCFDC0.CFDGAFLECTR.UINT8[1] = 0x00;   // Disable write to receive rule table
#endif
//--------- Receive Buffer settings -------------------------------------
   RCFDC0.CFDRMNB.UINT8[0] = 0x06;      // no. of receive buffers 
   
//--------- Receive Rule settings for RX Buffer -------------------------------------
#if 0
   //RSCAN0.GAFLCFG0.UINT8[3] = 0x02;   // No. of rules for channel 0
   //RSCAN0.GAFLECTR.UINT8[1] = 0x01;   // Enable write to receive rule table
   //RSCAN0.GAFLECTR.UINT8[0] = 0x00;   // receive rule page no.configuration
        //Receive Rule Configuration Register 0
   RCFDC0.CFDGAFLCFG0.UINT8[HH] = 0x02;   // No. of rules for channel 0
   //Receive Rule Entry Control Register
   RCFDC0.CFDGAFLECTR.UINT8[LH]= 0x01;   // Enable write to receive rule table
   //Receive rule entry control register
   RCFDC0.CFDGAFLECTR.UINT8[LL] = 0x00;   // receive rule page no.configuration   
   
// receive rule 1         
   //RSCAN0.GAFLID0.UINT16[0] = 0x0111;   // Standard, Data frame, 11 bit ID
   //RSCAN0.GAFLM0.UINT32 = 0x00;      // ID bits are not compared compared
   //RSCAN0.GAFLM0.UINT32 = 0xff;      // ID bits are compared compared
   //RSCAN0.GAFLP00.UINT8[1] = 0x84;      // receive buffer 4 used for storing message
   
        //Receive Rule ID Register
   RCFDC0.CFDGAFLID0.UINT16[L] = 0x0333;   // Standard, Data frame, 11 bit ID
   //RSCAN0.GAFLM0.UINT32 = 0x00;      // ID bits are not compared compared
   //Receive Rule Mask Register
   RCFDC0.CFDGAFLM0.UINT32 = 0xffffffff;   // ID bits are compared compared
   //Receive Rule Pointer 0 Register
   RCFDC0.CFDGAFLP0_0.UINT8[1] = 0x84;   //A receive buffer is used.
                                           //Receive buffer 4 used for storing message
   
// receive rule 2   
//   RSCAN0.GAFLID1.UINT16[0] = 0x0222;   // Standard, Data frame, 11 bit ID
   RCFDC0.CFDGAFLID1.UINT16[L] = 0x0844;   // Extended, Data frame, 29 bit ID
   RCFDC0.CFDGAFLID1.UINT16[H] = 0x8000;   // Extended, Data frame, 29 bit ID   
//   //RSCAN0.GAFLM0.UINT32 = 0x00;      // ID bits are not compared compared
//   RSCAN0.GAFLM1.UINT32 = 0xff;      // ID bits are compared compared
   RCFDC0.CFDGAFLM1.UINT32 = 0xffffffff;   // ID bits are compared compared
//   RSCAN0.GAFLP01.UINT8[1] = 0x84;      // receive buffer 5 used for storing message
   RCFDC0.CFDGAFLP0_1.UINT8[1] = 0x85;   // A receive buffer is used.
   
//   RSCAN0.GAFLECTR.UINT8[1] = 0x00;   // Disable write to receive rule table
   //Receive Rule Entry Control Register
   RCFDC0.CFDGAFLECTR.UINT8[LH]= 0x00;   // Disable write to receive rule table
#endif
   
//--------- Receive FIFO Buffer settings -------------------------------------
#if 0   
   //RSCAN0.RFCC0.UINT8[1] = 0x13;      // Interrupt on every message, Receive FIFO has 16 message depth
   RSCAN0.RFCC0.UINT8[1] = 0x03;      // Interrupt on 1/8 message received in FIFO, Receive FIFO has 16 message depth
   RSCAN0.RFCC0.UINT8[0] = 0x03;      // Receive FIFO buffers used and interrupt enabled
#endif   
//-----------Transmit/receive FIFO settings in Transmit mode -----------------
#if 0   
   RSCAN0.CFCC0.UINT8[2] = 0x61;      // Transmit mode selected, transmit buffer 6 linked to FIFO
   RSCAN0.CFCC0.UINT8[1] = 0x12;      // interrupt on every message transmitted, FIFO depth 8 messages
   RSCAN0.CFCC0.UINT8[0] = 0x04;      // transmit/receive FIFO transmit interupt enabled
#endif   
//-----------Transmit/receive FIFO settings in Receive mode -----------------
#if 0   
        //Transmit/receive FIFO Buffer Configuration and Control Register
   RCFDC0.CFDCFCC0.UINT8[2] = 0x00;      // Receive mode selected
   RCFDC0.CFDCFCC0.UINT8[1] = 0x13;      // interrupt on every message transmitted, FIFO depth 16 messages
   //RSCAN0.CFCC0.UINT8[1] = 0x03;      // Interrupt on 1/8 message received in FIFO, Receive FIFO has 16 message depth
   RCFDC0.CFDCFCC0.UINT8[0] = 0x03;      // transmit/receive FIFO transmit interupt enabled
#endif   
//----------- Global Interrupt -------------------------------------
   
   RCFDC0.CFDGCTR.UINT8[LH] = 0x07;      // Transmit history interrupt, FIFO msg lost interrupt & DLC error interrupt enabled
   
   //RSCAN0.C0CTR.UINT16[1] = 0x0001;   // Trasmit abort interrupt enabled
   //RSCAN0.C0CTR.UINT8[1] = 0x01;      // Bus error interrupt enabled
   
//------------ Interrupt setting -----------------------------------

//NDG commented
   INTC1.ICRCAN0ERR.BIT.TBRCAN0ERR = 1;      // 0-Direct jump method, 1-vector method
   INTC1.ICRCAN0ERR.BIT.MKRCAN0ERR = 0;
   INTC1.ICRCAN0ERR.BIT.RFRCAN0ERR = 0;
    
   INTC1.ICRCAN0TRX.BIT.TBRCAN0TRX = 1;
   INTC1.ICRCAN0TRX.BIT.MKRCAN0TRX = 0;
   INTC1.ICRCAN0TRX.BIT.RFRCAN0TRX = 0;
 
   INTC1.ICRCAN0REC.BIT.TBRCAN0REC = 1;
   INTC1.ICRCAN0REC.BIT.MKRCAN0REC = 0;
   INTC1.ICRCAN0REC.BIT.RFRCAN0REC = 0;
   
   INTC1.ICRCANGERR0.BIT.TBRCANGERR0 = 1;
   INTC1.ICRCANGERR0.BIT.MKRCANGERR0 = 0;
   INTC1.ICRCANGERR0.BIT.RFRCANGERR0 = 0;

   INTC1.ICRCANGRECC0.BIT.TBRCANGRECC0 = 1;
   INTC1.ICRCANGRECC0.BIT.MKRCANGRECC0 = 0;
   INTC1.ICRCANGRECC0.BIT.RFRCANGRECC0 = 0;
      
   
//------------ Operating mode --------------------------------------
   
   RCFDC0.CFDGCTR.UINT8[0] = 0x00;      //Other than global stop mode
                                           //Global operating mode
   for(i=0;i<0xfff;i++);   //wait for transistion
        
   //Set RFE bit in global operating mode 
   RCFDC0.CFDRFCC0.UINT8[0] |= 0x01;      // receive FIFO is used
   //RCFDC0.CFDRFCC0.UINT16[L] |= 0x01;      // receive FIFO is used
   
   RCFDC0.CFDC0CTR.UINT8[0] = 0x00;   //other than channel stop mode
                                           //channel communication mode
   for(i=0;i<0xfff;i++);   //wait for transistion
   
   //commented because currently not using FIFO
   //RCFDC0.CFDCFCC0.UINT8[0] |= 0x01;      // transmit/receive FIFO is used
}

//**************************************************************************************************************************

void can_tx(uint16_t can_id,uint8_t input[8])
{
//----------- trnasmit using transmit buffer ---------------------------------------------   
#if 1
   if(!(RCFDC0.CFDTMSTS0 & 0x01))
   {       
      //Transmit buffer interrupt enable configuration register
      RCFDC0.CFDTMIEC0.UINT8[0] = 0x01;      // Transmit Buffer 0 interrupt enabled
      
      //Transmit buffer ID register
      RCFDC0.CFDTMID0.UINT16[0] = can_id;   // Transmit message, standard data frame, 
      //Transmit buffer pointer register
  //  RCFDC0.CFDTMPTR0.UINT16[1] = 0x8012;   // 8 data bytes, label value as 0x12
  
      RCFDC0.CFDTMPTR0.UINT16[1] = 0xF012;   // 64 data bytes, label value as 0x12  
  
 //   RCFDC0.CFDTMFDCTR0.UINT8[LL] = 0x04;   // TO CONFIGURE FRAME MODE FOR CANFD 
      RCFDC0.CFDTMFDCTR0.UINT8[LL] = 0x00;   // TO CONFIGURE FRAME MODE FOR NORMAL CAN
      
      //Transmit buffer data field register
      
      RCFDC0.CFDTMDF0_0.UINT32 =  ((uint32_t)input[3] << 24) | ((uint32_t)input[2] << 16) | ((uint32_t)input[1] << 8) | input[0];  // data bytes 0 to 3
      
      RCFDC0.CFDTMDF1_0.UINT32 = ((uint32_t)input[7] << 24) | ((uint32_t)input[6] << 16) | ((uint32_t)input[5] << 8) | input[4];   // data bytes 4 to 7
         
      
      //Transmit buffer status register
      //if((RCFDC0.CFDTMSTS0 &0x01) == 0x00)   //check if no other transmit request is present or transmission is in progress
      RCFDC0.CFDTMC0 = 0x01;         // Transmission requested - TMTR bit
      //tx_done = 0;
   }
   
#endif
//----------- trnasmit using transmit/receive FIFO ---------------------------------------------
#if 0
   
   //RSCAN0.CFCC0.UINT8[0] |= 0x01;      // transmit/receive FIFO enabled
   
   RSCAN0.CFID0.UINT16[0] = 0x0111;   // data frame, standard ID
   
   RSCAN0.CFPTR0.UINT8[3] = 0x80;      // DLC of 8 data bytes
   
   RSCAN0.CFDF00.UINT32 = 0xAAAAAAAA;   // data bytes 0 to 3
   RSCAN0.CFDF10.UINT32 = 0xBBBBBBBB;   // data bytes 4 to 7

   RSCAN0.CFPCTR0.UINT8[0] = 0xff;
   //////////////////////////////////
   
   RSCAN0.CFID0.UINT16[0] = 0x0222;   // data frame, standard ID
   
   RSCAN0.CFPTR0.UINT8[3] = 0x80;      // DLC of 8 data bytes
   
   RSCAN0.CFDF00.UINT32 = 0xCCCCCCCC;   // data bytes 0 to 3
   RSCAN0.CFDF10.UINT32 = 0xDDDDDDDD;   // data bytes 4 to 7

   RSCAN0.CFPCTR0.UINT8[0] = 0xff;
   //////////////////////////////////
   
   RSCAN0.CFID0.UINT16[0] = 0x0333;   // data frame, standard ID
   
   RSCAN0.CFPTR0.UINT8[3] = 0x80;      // DLC of 8 data bytes
   
   RSCAN0.CFDF00.UINT32 = 0xEEEEEEEE;   // data bytes 0 to 3
   RSCAN0.CFDF10.UINT32 = 0xFFFFFFFF;   // data bytes 4 to 7

   RSCAN0.CFPCTR0.UINT8[0] = 0xff;
   //////////////////////////////////
#endif
}

//**************************************************************************************************************************
unsigned char rx_msg_DLC;
unsigned int rx_msg_ID;
unsigned long rx_msg_data0, rx_msg_data1;
unsigned long rx_msg_data2;
unsigned long rx_msg_data3; 
unsigned long rx_msg_data4; 
unsigned long rx_msg_data5;
unsigned long rx_msg_data6;
unsigned long rx_msg_data7,  rx_msg_data8,  rx_msg_data9,  rx_msg_data10, rx_msg_data11,
	      rx_msg_data12, rx_msg_data13, rx_msg_data14, rx_msg_data15;

//typedef struct 
//{
//unsigned char rx_msg_DLC;
//unsigned int rx_msg_ID;
//unsigned long rx_msg_data0;   
//unsigned long rx_msg_data1;
   
//}RX_fifo_data;

RX_fifo_data Recd_RX_fifo_data_arr[8];

//**************************************************************************************************************************

void my_can0_rx(void)
{
//-------------------- Rx by reading Message buffer-------------------------------------------------------   
#if 1
   if((RCFDC0.CFDRMND0.UINT8[0] & 0x10) == 0x10)   // check if RMNS bit of RMND register is set to '1'
   {
      RCFDC0.CFDRMND0.UINT8[0] &= 0xEF;      // clear RMNS bit
      
      rx_msg_ID = RCFDC0.CFDRMID4.UINT16[0];      // read received message ID
      rx_msg_DLC = (RCFDC0.CFDRMPTR4.UINT8[3] & 0xF0) >> 4;   // read received message data length
      
      rx_msg_data0 = RCFDC0.CFDRMDF0_4.UINT32;
      rx_msg_data1 = RCFDC0.CFDRMDF1_4.UINT32;
      
   }
               if((RCFDC0.CFDRMND0.UINT8[0] & 0x20) == 0x20)   // check if RMNS bit of RMND register is set to '1'
   {
      RCFDC0.CFDRMND0.UINT8[0] &= 0xDF;      // clear RMNS bit
      
      if(RCFDC0.CFDRMID5.UINT8[3] & 0x80 == 0x80 )    // check if extended or standard ID
      {
      rx_msg_ID = RCFDC0.CFDRMID5.UINT16[0];      // read received message ID
      }
      else
      {
         //return error as the expected ID is extended   
         __nop();
      }
      rx_msg_DLC = (RCFDC0.CFDRMPTR5.UINT8[3] & 0xF0) >> 4;   // read received message data length
      
      rx_msg_data0 = RCFDC0.CFDRMDF0_5.UINT32;
      rx_msg_data1 = RCFDC0.CFDRMDF1_5.UINT32;
      
   }
#endif
//-------------------- Rx by reading Rx FIFO buffer-------------------------------------------------------   
#if 1
   RCFDC0.CFDRFSTS0.UINT8[0] &= 0xF7;   // CLEAR RFIF flag
   if((RCFDC0.CFDRFSTS0.UINT8[0] & 0x01) != 0x01)
   {
      do
      {
         rx_msg_ID = RCFDC0.CFDRFID0.UINT16[0];
         rx_msg_DLC = (RCFDC0.CFDRFPTR0.UINT8[3] & 0xf0) >> 4;
         
         rx_msg_data0 = RCFDC0.CFDRFDF0_0.UINT32;
         rx_msg_data1 = RCFDC0.CFDRFDF1_0.UINT32;
//for DLC = 64	 
	 rx_msg_data2 = RCFDC0.CFDRFDF2_0.UINT32;
	 rx_msg_data3 = RCFDC0.CFDRFDF3_0.UINT32;
	 rx_msg_data4 = RCFDC0.CFDRFDF4_0.UINT32;
	 rx_msg_data5 = RCFDC0.CFDRFDF5_0.UINT32;
	 rx_msg_data6 = RCFDC0.CFDRFDF6_0.UINT32;
	 rx_msg_data7 = RCFDC0.CFDRFDF7_0.UINT32;
	 rx_msg_data8 = RCFDC0.CFDRFDF8_0.UINT32;
	 rx_msg_data9 = RCFDC0.CFDRFDF9_0.UINT32;
	 rx_msg_data10 = RCFDC0.CFDRFDF10_0.UINT32;
	 rx_msg_data11 = RCFDC0.CFDRFDF11_0.UINT32;
	 rx_msg_data12 = RCFDC0.CFDRFDF12_0.UINT32;
	 rx_msg_data13 = RCFDC0.CFDRFDF13_0.UINT32;
	 rx_msg_data14 = RCFDC0.CFDRFDF14_0.UINT32;
	 rx_msg_data15 = RCFDC0.CFDRFDF15_0.UINT32;
	 
	 
	 
	 
         
         Recd_RX_fifo_data_arr[0].rx_msg_ID = RCFDC0.CFDRFID0.UINT16[0];
         Recd_RX_fifo_data_arr[0].rx_msg_DLC = (RCFDC0.CFDRFPTR0.UINT8[3] & 0xf0) >> 4;
         
         Recd_RX_fifo_data_arr[0].rx_msg_data0 = RCFDC0.CFDRFDF0_0.UINT32;
         Recd_RX_fifo_data_arr[0].rx_msg_data1 = RCFDC0.CFDRFDF1_0.UINT32;         
         
         RCFDC0.CFDRFPCTR0.UINT8[0] = 0xFF;
      }while((RCFDC0.CFDRFSTS0.UINT8[0] & 0x01) != 0x01);// check if FIFO empty
   }
#endif

//-------------------- Rx by reading Tx/Rx FIFO buffer-------------------------------------------------------   
#if 0
   RCFDC0.CFDCFSTS0.UINT8[0] &= 0xF7;   // CLEAR CFRXIF flag
   if((RCFDC0.CFDCFSTS0.UINT8[0] & 0x01) != 0x01)
   {
      do
      {
         rx_msg_ID = RCFDC0.CFDCFID0.UINT16[0];
         rx_msg_DLC = (RCFDC0.CFDCFPTR0.UINT8[3] & 0xf0) >> 4;
         
         rx_msg_data0 = RCFDC0.CFDCFDF0_0.UINT32;
         rx_msg_data1 = RCFDC0.CFDCFDF1_0.UINT32;
         
         RCFDC0.CFDCFPCTR0.UINT8[0] = 0xFF;
      }while((RCFDC0.CFDCFSTS0.UINT8[0] & 0x01) != 0x01);
   }
#endif
//--------------------------------------------------------------------------------------------------------
}

//**************************************************************************************************************************

#pragma interrupt CAN_Global_Error_ISR(enable=false, channel=22, fpu=true, callt=false)
 void CAN_Global_Error_ISR(void)
{
     //__nop();
     if(RCFDC0.CFDRFSTS0.UINT8[0] & 0x04)  // check if FIFO message is lost
     {
        RX_Fifo_msg_lost = 1;
        //return;
     }
     
}

//**************************************************************************************************************************

#pragma interrupt CAN_Rx_FIFO_ISR(enable=false, channel=23, fpu=true, callt=false)
 void CAN_Rx_FIFO_ISR(void)
{
     __nop();
    // rx_flag = 1;
     RCFDC0.CFDRFSTS0.UINT8[0] &= 0xF7;   // CLEAR RFIF flag

     if(RCFDC0.CFDRFSTS0.UINT8[0] & 0x01)  // check if FIFO empty
     {
             RX_Fifo_empty = 1;   
        return;
     }
     else if(RCFDC0.CFDRFSTS0.UINT8[0] & 0x02) // check if FIFO full
     {
        RX_fifo_full = 1;     
     }
//     else if(RCFDC0.CFDRFSTS0.UINT8[0] & 0x04)  // check if FIFO message is lost
//     {
//        RX_Fifo_msg_lost = 1;
//        return;
//     }     
     my_can0_rx();
}

//**************************************************************************************************************************

#pragma interrupt CAN0_Error_ISR(enable=false, channel=24, fpu=true, callt=false)
 void CAN0_Error_ISR(void)
{
     __nop();
}

//**************************************************************************************************************************

#pragma interrupt CAN0_Tx_Rx_FIFO_Receive_ISR(enable=false, channel=25, fpu=true, callt=false)
 void CAN0_Tx_Rx_FIFO_Receive_ISR(void)
{
     __nop();
     //rx_flag = 1;
     RCFDC0.CFDCFSTS0.UINT8[0] &= 0xF7;   // CLEAR Interrupt flag   
}

//**************************************************************************************************************************

#pragma interrupt CAN0_Tx_ISR(enable=false, channel=26, fpu=true, callt=false)
 void CAN0_Tx_ISR(void)
{

#if TX_MODE == TX_BUFFER   
   //RCFDC0.CFDTMSTS0 = 0x00;
   RCFDC0.CFDTMSTS0 = 0x00;
   __nop();
#else                  // TX_RX_FIFO   
   //RCFDC0.CFDCFSTS0.UINT8[0] &= 0xE7;   //clear interrupt flags
   RCFDC0.CFDCFSTS0.UINT8[0] &= 0xE7;   //clear interrupt flags
#endif
   INTC1.ICRCAN0TRX.BIT.RFRCAN0TRX = 0;
   __nop();
   //tx_done = 1;
     
}

//**************************************************************************************************************************

