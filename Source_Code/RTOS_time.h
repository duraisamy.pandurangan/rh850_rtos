#ifndef _RTOS_TIMER_H
#define _RTOS_TIMER_H

#define 

// Simulated LED toggle function
void ToggleLED() {
    static int state = 0;
    state = !state; // Toggle the state
    printf("LED State: %s\n", state ? "ON" : "OFF");
    // Simulate toggling an actual LED
}

// Timer callback function to toggle the LED
void TimerCallback(TimerHandle_t pxTimer) {
    ToggleLED();
}

void TimerTask(void *pvParameters) {
    // Create a timer with a 1000ms (1-second) period
    TimerHandle_t myTimer = xTimerCreate("MyTimer", pdMS_TO_TICKS(1000), pdTRUE, 0, TimerCallback);

    if (myTimer == NULL) {
        printf("Failed to create timer.\n");
        vTaskDelete(NULL); // Exit the task if the timer creation fails
    }

    // Start the timer
    if (xTimerStart(myTimer, 0) != pdPASS) {
        printf("Failed to start timer.\n");
        vTaskDelete(NULL); // Exit the task if the timer start fails
    }

    while (1) {
        // Your main task code goes here
        vTaskDelay(pdMS_TO_TICKS(500)); // Simulate other work in the task
    }
}

#endif