/**
 * @file
 * @brief Header file for binary semaphore example.
 */

#ifndef R_SEMAPHORE_H
#define R_SEMAPHORE_H

#define SEM_BIN
#define COUNT_SEM

#ifdef SEM_BIN
/**
 * @defgroup BinarySemaphoreTask Binary Semaphore Task
 * @{
 */


/**
 * @brief Structure to hold shared data.
 */
typedef struct {
    int sharedData; /**< Shared integer data. */
} SharedData;


// Create shared data structure
  SharedData sharedData;
 
/**
 * @brief Task1 function to modify the shared data.
 *
 * This task acquires a semaphore to protect the shared data, increments
 * the shared data, and prints the updated value.
 *
 * @param pvParameters Pointer to the SharedData structure.
 */
void Task1(void *pvParameters) {
    SharedData* sharedData = (SharedData*)pvParameters;

    while (1) {
        // Acquire the semaphore to protect shared data
        xSemaphoreTake(dataSemaphore, portMAX_DELAY);

        // Modify the shared data
        sharedData->sharedData++;
        printf("Task1: Incremented shared data to %d\n", sharedData->sharedData);

        // Release the semaphore
        xSemaphoreGive(dataSemaphore);

        // Wait for some time before modifying data again
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}

/**
 * @brief Task2 function to read the shared data.
 *
 * This task acquires a semaphore to protect the shared data, reads and prints
 * the shared data, and then releases the semaphore.
 *
 * @param pvParameters Pointer to the SharedData structure.
 */
void Task2(void *pvParameters) {
    SharedData* sharedData = (SharedData*)pvParameters;

    while (1) {
        // Acquire the semaphore to protect shared data
        xSemaphoreTake(dataSemaphore, portMAX_DELAY);

        // Read and print the shared data
        printf("Task2: Shared data is %d\n", sharedData->sharedData);

        // Release the semaphore
        xSemaphoreGive(dataSemaphore);

        // Wait for some time before reading data again
        vTaskDelay(pdMS_TO_TICKS(2000));
    }
}

/**
 * @brief Function to create and initialize a binary semaphore and associated tasks.
 *
 * This function creates a binary semaphore (`dataSemaphore`) and initializes shared data.
 * It then creates Task1 and Task2, passing the shared data to each task.
 * 
 * @note Ensure that `dataSemaphore`, `Task1`, and `Task2` are defined globally or within the scope of this function.
 */
 
void create_BIN_semaphore(){


	    // Create a binary semaphore
    dataSemaphore = xSemaphoreCreateBinary();

    // Check if semaphore creation was successful
    if (dataSemaphore == NULL) {
        printf("Failed to create semaphore\n");
        return;
    }

    
  sharedData.sharedData = 0;

    // Create Task1 and Task2
    xTaskCreate(Task1, "Task1", configMINIMAL_STACK_SIZE, (void*)&sharedData, 1, NULL);
    xTaskCreate(Task2, "Task2", configMINIMAL_STACK_SIZE, (void*)&sharedData, 2, NULL);

	
}
#endif
/*End of Binary Semaphore task*/

#ifdef COUNT_SEM

/**
 * @def MAX_COUNT
 * @brief Maximum count value for the shared counter.
 *
 * This macro defines the maximum count value that the shared counter can reach.
 */
#define MAX_COUNT 10

SemaphoreHandle_t countSemaphore;
int sharedCounter = 0;
/**
 * @brief Task to increment a shared counter using a counting semaphore.
 *
 * This task repeatedly acquires the counting semaphore to protect the shared counter,
 * increments the counter if it is less than the maximum count, and prints the updated value.
 * After modification, the semaphore is released, and the task waits for some time before incrementing again.
 *
 * @param pvParameters Unused parameter.
 */
void IncrementTask(void *pvParameters) {
    while (1) {
        xSemaphoreTake(countSemaphore, portMAX_DELAY);

        if (sharedCounter < MAX_COUNT) {
            sharedCounter++;
            printf("IncrementTask: Counter incremented to %d\n", sharedCounter);
        }

        xSemaphoreGive(countSemaphore);

        vTaskDelay(pdMS_TO_TICKS(500)); // Wait for some time before incrementing again
    }
}

/**
 * @brief Task to decrement a shared counter using a counting semaphore.
 *
 * This task repeatedly acquires the counting semaphore to protect the shared counter,
 * decrements the counter if it is greater than 0, and prints the updated value.
 * After modification, the semaphore is released, and the task waits for some time before decrementing again.
 *
 * @param pvParameters Unused parameter.
 */
void DecrementTask(void *pvParameters) {
    while (1) {
        xSemaphoreTake(countSemaphore, portMAX_DELAY);

        if (sharedCounter > 0) {
            sharedCounter--;
            printf("DecrementTask: Counter decremented to %d\n", sharedCounter);
        }

        xSemaphoreGive(countSemaphore);

        vTaskDelay(pdMS_TO_TICKS(800)); // Wait for some time before decrementing again
    }
}

/**
 * @brief Function to create a counting semaphore and associated tasks.
 *
 * This function creates a counting semaphore (`countSemaphore`) with a maximum count of `MAX_COUNT` and an initial count of `0`.
 * It then creates tasks (`IncrementTask` and `DecrementTask`) that use the counting semaphore to synchronize access to a shared counter.
 */
void Create_counterSEM(){
	
 countSemaphore = xSemaphoreCreateCounting(MAX_COUNT, 0);

    if (countSemaphore != NULL) {
        xTaskCreate(IncrementTask, "IncrementTask", configMINIMAL_STACK_SIZE, NULL, 1, NULL);
        xTaskCreate(DecrementTask, "DecrementTask", configMINIMAL_STACK_SIZE, NULL, 2, NULL);
    }
}
#endif 

void Create_semaphore(){
	
	create_BIN_semaphore();
	Create_counterSEM();
	
}
	
#endif