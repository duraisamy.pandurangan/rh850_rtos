#ifndef _IPC_H
#define _IPC_H



typedef struct {
    char message[20];
} Message;



// Define a handle for a binary semaphore to protect access to shared data
SemaphoreHandle_t dataSemaphore;

  
  // Define a handle for the message queue
QueueHandle_t messageQueue;
Message receivedMsg;/*This will helpful for watch window when its global*/
 
static void copyString(char* dest, const char* src, size_t maxLen) {
    strncpy(dest, src, maxLen - 1);
    dest[maxLen - 1] = '\0';  // Ensure null-termination
}

// Message_Task1 function
static void Message_Task1(void *pvParameters) {
    Message msg;
    
    while (1) {
        // Prepare a message
         copyString(msg.message, "Hello from Task1", 20);

        
        // Send the message to the queue
        xQueueSend(messageQueue, &msg, portMAX_DELAY);
        
        // Wait for some time before sending the next message
       vTaskDelay(100/portTICK_RATE_MS);
    }
}

// Message_Task2 function
static void Message_Task2(void *pvParameters) {
   // Message receivedMsg;
    
    while (1) {
        // Receive a message from the queue
        if (xQueueReceive(messageQueue, &receivedMsg, portMAX_DELAY)) {
            // Process the received message
	    PORT.P9 = TOGGLE_GPIO(PORT.P9,1);	   
            printf("Task2 received: %s\n", receivedMsg.message);
        }
    }
}

void Create_IPC()
{
	
	 messageQueue = xQueueCreate(5, sizeof(Message));
    // Create Task1 and Task2
    xTaskCreate(Message_Task1, "Task1", TASK_ONE_STACK_SIZE, NULL, 1, NULL);
    xTaskCreate(Message_Task2, "Task2", TASK_ONE_STACK_SIZE, NULL, 2, NULL); 
}
//#endif
#endif