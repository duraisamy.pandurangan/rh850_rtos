
/**
 * @file R_TaskScheduler.h
 * @brief Header file for the Task Scheduler module.
 */

#ifndef TASKSCHEDULER_H
#define TASKSCHEDULER_H

#include "r_cg_macrodriver.h"
#include "r_cg_userdefine.h"
#include "Config_OSTM0.h"
#include "Config_PORT.h"
#include "r_cg_cgc.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "event_groups.h"

#include <stdio.h>


// Define event bit flags
#define EVENT_BIT_1    (1 << 0)
#define EVENT_BIT_2    (1 << 1)


/**
 * @def TOGGLE_GPIO(var, bit)
 * @brief Macro for toggling a GPIO bit.
 * @param var Variable representing the GPIO port.
 * @param bit Bit number to toggle.
 * @note For GPIO implementation, refer to r_cg_port.
 */
#define TOGGLE_GPIO(var, bit) ((var) ^= (1 << (bit)))

/**
 * @def Task_Schedule
 * @brief Macro to enable task scheduling.
 */
//#define Task_evengroup

#ifdef Task_evengroup
EventGroupHandle_t eventGroup;

void Event_Task1(void *pvParameters) {
    while (1) {
        // Wait for EVENT_BIT_1 to be set in the event group
        EventBits_t bits = xEventGroupWaitBits(eventGroup, EVENT_BIT_1, pdTRUE, pdFALSE, portMAX_DELAY);

        if (bits & EVENT_BIT_1) {
            printf("Task1: Received Event Bit 1\n");
            // Perform task actions for Event Bit 1
        }
    }
}

void Event_Task2(void *pvParameters) {
    while (1) {
        // Wait for EVENT_BIT_2 to be set in the event group
        EventBits_t bits = xEventGroupWaitBits(eventGroup, EVENT_BIT_2, pdTRUE, pdFALSE, portMAX_DELAY);

        if (bits & EVENT_BIT_2) {
            printf("Task2: Received Event Bit 2\n");
            // Perform task actions for Event Bit 2
        }
    }
}

void EventProducerTask(void *pvParameters) {
    while (1) {
        // Simulate an event by setting EVENT_BIT_1
        xEventGroupSetBits(eventGroup, EVENT_BIT_1);

        vTaskDelay(pdMS_TO_TICKS(1000));  // Wait for 1 second

        // Simulate another event by setting EVENT_BIT_2
        xEventGroupSetBits(eventGroup, EVENT_BIT_2);

        vTaskDelay(pdMS_TO_TICKS(1000));  // Wait for 1 second
    }
}


#else 
/**
 * @brief RTOS task handlers.
 */
static TaskHandle_t LED_1_handle1 = NULL; /**< Handle for LED 1 task. */
static TaskHandle_t LED_2_handle2 = NULL; /**< Handle for LED 2 task. */
static TaskHandle_t LED_3_handle3 = NULL; /**< Handle for LED 3 task. */

/**
 * @brief LED 1 task function.
 * @param pvParameters Task parameters (not used).
 */
static void vLED_1_Task(void *pvParameters);

/**
 * @brief LED 2 task function.
 * @param pvParameters Task parameters (not used).
 */
static void vLED_2_Task(void *pvParameters);

/**
 * @brief LED 3 task function.
 * @param pvParameters Task parameters (not used).
 */
static void vLED_3_Task(void *pvParameters);

/**
 * @brief Variable to count the number of cycles for LED 1.
 */
unsigned int testcount1 = 0;

/**
 * @brief Variable to count the number of cycles for LED 2.
 */
unsigned int testcount2 = 0;

/**
 * @brief Variable to count the number of cycles for LED 3.
 */
unsigned int testcount3 = 0;

/**
 * @brief LED 1 task function implementation.
 * @param pvParameters Task parameters (not used).
 */
static void vLED_1_Task(void *pvParameters);

/**
 * @brief LED 2 task function implementation.
 * @param pvParameters Task parameters (not used).
 */
static void vLED_2_Task(void *pvParameters);

/**
 * @brief LED 3 task function implementation.
 * @param pvParameters Task parameters (not used).
 */
static void vLED_3_Task(void *pvParameters);



/**
 * @brief LED 1 task function.
 *
 * This function toggles the state of a GPIO pin, delays for a specified time,
 * and increments a counter. It runs indefinitely in a FreeRTOS task.
 *
 * @param pvParameters Task parameters (not used).
 */
static void vLED_1_Task(void *pvParameters);

/**
 * @brief LED 1 task function implementation.
 *
 * This function toggles the state of a specific GPIO pin (PORT.P9, bit 0),
 * delays for a specified time (1 millisecond in this case), and increments
 * a counter. The task repeats indefinitely, and the counter is reset when it
 * reaches 2000.
 *
 * @param pvParameters Task parameters (not used).
 */
static void vLED_1_Task(void *pvParameters)
{
    TickType_t xLastWakeTime;

    // Initialize the last wake time with the current tick count.
    xLastWakeTime = xTaskGetTickCount();

    for (;;)
    {
        // Toggle the state of PORT.P9, bit 0.
        PORT.P9 = TOGGLE_GPIO(PORT.P9, 0);

        // Delay for 1 millisecond.
        vTaskDelay(1 / portTICK_RATE_MS);
	    //vTaskDelayUntil( &xLastWakeTime, xFrequency );
        //vTaskDelayUntil(&xLastWakeTime, RTOS_TIMING_10MS);

        // Increment the counter.
        testcount1++;

        // Reset the counter when it reaches 2000.
        if (testcount1 == 2000)
        {
            testcount1 = 0;
	 vTaskSuspend(LED_1_handle1) ; 
	    
        }
    }

    // Delete the task (should not reach this point).
    vTaskDelete(NULL);
}


/**
 * @brief LED 2 task function.
 *
 * This function toggles the state of a GPIO pin, delays for a specified time,
 * and increments a counter. It runs indefinitely in a FreeRTOS task.
 *
 * @param pvParameters Task parameters (not used).
 */
static void vLED_2_Task(void *pvParameters);

/**
 * @brief LED 2 task function implementation.
 *
 * This function toggles the state of PORT.P9, bit 1, delays for 1 millisecond,
 * increments a counter, and resets the counter when it reaches 2000. The task
 * runs indefinitely in a FreeRTOS loop.
 *
 * @param pvParameters Task parameters (not used).
 */
static void vLED_2_Task(void *pvParameters)
{
    TickType_t xLastWakeTime;

    // Initialize the last wake time with the current tick count.
    xLastWakeTime = xTaskGetTickCount();

    for (;;)
    {
        // Toggle the state of PORT.P9, bit 1.
        PORT.P9 = TOGGLE_GPIO(PORT.P9, 1);
	//vTaskDelayUntil( &xLastWakeTime, xFrequency );
        //vTaskDelayUntil(&xLastWakeTime, RTOS_TIMING_10MS);
        // Delay for 1 millisecond.
        vTaskDelay(1 / portTICK_RATE_MS);

        // Increment the counter.
        testcount2++;

        // Reset the counter when it reaches 2000.
        if (testcount2 == 4000)
        {
            testcount2 = 0;
	 vTaskResume(LED_1_handle1) ; 
	    
        }
    }

    // Delete the task (should not reach this point).
    vTaskDelete(NULL);
}


/**
 * @brief LED 3 task function implementation.
 *
 * This function delays for 1 millisecond, toggles the state of PORT.P9, bit 2,
 * increments a counter, and resets the counter when it reaches 2000. The task
 * runs indefinitely in a FreeRTOS loop.
 *
 * @param pvParameters Task parameters (not used).
 */
static void vLED_3_Task(void *pvParameters)
{
    TickType_t xLastWakeTime;

    // Initialize the last wake time with the current tick count.
    xLastWakeTime = xTaskGetTickCount();

    for (;;)
    {
        // Delay for 1 millisecond.
        vTaskDelay(1 / portTICK_RATE_MS);

        // Toggle the state of PORT.P9, bit 2.
        PORT.P9 = TOGGLE_GPIO(PORT.P9, 2);

        // Increment the counter.
        testcount3++;

        // Reset the counter when it reaches 2000.
        if (testcount3 == 2000)
        {
            testcount3 = 0;
        }
    }

    // Delete the task (should not reach this point).
    vTaskDelete(NULL);
}







/**
 * @brief Create example tasks.
 *
 * This function creates three example tasks, each corresponding to an LED.
 * The tasks are created with different stack sizes and priorities.
 */
 #endif
void Taskcreate_example(){
	
 #ifdef	Task_evengroup
     eventGroup = xEventGroupCreate();
    if (eventGroup != NULL) {
        // Create tasks
        xTaskCreate(Event_Task1, "Task1", configMINIMAL_STACK_SIZE, NULL, 1, NULL);
        xTaskCreate(Event_Task2, "Task2", configMINIMAL_STACK_SIZE, NULL, 2, NULL);
        xTaskCreate(EventProducerTask, "EventProducerTask", configMINIMAL_STACK_SIZE, NULL, 3, NULL);
	
	
    }   
 #else   
 /* @code
 * xTaskCreate( vLED_1_Task, (signed portCHAR *)"LED1", TASK_ONE_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, &LED_1_handle1 );
 * @endcode
 * 
 * - Task for LED  is created using xTaskCreate.
 * - Task name is "LED".
 * - Stack size is TASK_ONE_STACK_SIZE.
 * - Task function is vLED__Task.
 * - Task priority is set to tskIDLE_PRIORITY.
 * - LED_1_handle1 stores the task handle.
 */
// vLED_1_Task has a relatively low priority.
 //vLED_3_Task has a medium priority.
 //vLED_2_Task has the highest priority among the three tasks.
 //Priority values are integers, and higher numerical values correspond 
    xTaskCreate( vLED_1_Task, ( signed portCHAR * ) "LED1", TASK_ONE_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, &LED_1_handle1 );  
    xTaskCreate( vLED_2_Task, ( signed portCHAR * ) "LED2", TASK_TWO_STACK_SIZE, NULL, tskIDLE_PRIORITY+3, &LED_2_handle2 );
    xTaskCreate( vLED_3_Task, ( signed portCHAR * ) "LED3", TASK_THREE_STACK_SIZE, NULL, tskIDLE_PRIORITY+2, &LED_3_handle3 );
#endif
}


/*********Event Group Task *************/



#endif
